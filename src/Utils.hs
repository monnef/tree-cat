module Utils
  ( Text,
    tshow,
    pShow,
    toS,
    (>>>),
    (&),
    ($>),
    (<&>),
    thenCmp,
  )
where

import Control.Arrow ((>>>))
import Data.Function ((&))
import Data.Functor (($>), (<&>))
import Data.String.Conv (toS)
import Data.Text (Text)
import qualified Data.Text as T
import Text.InterpolatedString.Perl6 (qq)
import qualified Text.Pretty.Simple as PrettySimple

pShow :: Show a => a -> Text
pShow = PrettySimple.pShowOpt opts >>> toS
  where
    opts = PrettySimple.defaultOutputOptionsDarkBg {PrettySimple.outputOptionsIndentAmount = 2}

tshow :: Show a => a -> Text
tshow = show >>> T.pack

thenCmp :: Ordering -> Ordering -> Ordering
thenCmp EQ o2 = o2
thenCmp o1 _ = o1
