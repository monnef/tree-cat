{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

module Data where

import Data.Default (Default, def)
import Optics (makeFieldLabelsWith, noPrefixFieldLabels)

data Node a v = Node {addr :: Maybe a, value :: Maybe v, children :: [Node a v]} deriving (Show)

makeFieldLabelsWith noPrefixFieldLabels ''Node

instance Default (Node a v) where
  def = Node {addr = Nothing, value = Nothing, children = []}
