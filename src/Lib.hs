{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Lib
  ( fmtTree,
    printTree,
    mkTree,
    descFromTree,
    sortDescriptions,
  )
where

import Control.Monad (join)
import Data
import Data.Bool (bool)
import Data.Default (def)
import Data.Function (on)
import Data.List (groupBy, sortBy, sortOn)
import Data.Maybe (fromMaybe, isNothing, maybeToList)
import qualified Data.Text as T
import Optics
import Safe (headMay)
import Utils

fmtTree :: (Show a, Show v) => Node a v -> Text
fmtTree = pShow

printTree :: (Show a, Show v) => Node a v -> IO ()
printTree x = do
  putStrLn $ x & fmtTree & toS
  putStrLn $ toS $ collectTree "" x
  where
    collectTree :: (Show a, Show v) => Text -> Node a v -> Text
    collectTree cAddr x =
      (if isNothing (x ^. #value) || isNothing (x ^. #addr) then "" else curT)
        <> (x ^. #children <&> collectTree wholeAddrT & filter (not . T.null) & T.intercalate "")
      where
        curT = wholeAddrT <> " = " <> (x ^. #value <&> tshow & fromMaybe "???") <> "\n"
        curAddrT = (x ^. #addr) <&> tshow & fromMaybe ""
        wholeAddrT = cAddr <> (bool "." "" $ T.null cAddr) <> curAddrT

cmpAddr :: Ord a => [a] -> [a] -> Ordering
cmpAddr = (compare `on` headMay) >.> (compare `on` length) >.> compare
  where
    a >.> b = \x y -> a x y `thenCmp` b x y

sortDescriptions :: Ord a => [([a], v)] -> [([a], v)]
sortDescriptions = sortBy (cmpAddr `on` fst)

mkBranch :: (Eq a, Ord a) => Maybe a -> Maybe v -> [([a], v)] -> Node a v
mkBranch curAddr currValue rest = Node {addr = curAddr, value = currValue, children = children}
  where
    -- [[([a], v)]]
    grouped = rest & sortOn (fst >>> headMay) & groupBy ((==) `on` fst >>> headMay) <&> sortBy (cmpAddr `on` fst)
    children = grouped <&> processGroup
    -- [([a], v)]
    processGroup :: (Ord a) => [([a], v)] -> Node a v
    processGroup (([a], v) : xs) =
      Node
        { addr = Just a,
          value = Just v,
          children = xs <&> (_1 %~ tail) <&> (: []) <&> mkBranch Nothing Nothing <&> (^. #children) & join
        }
    processGroup h@((a : as, v) : xs) = mkBranch (Just a) Nothing (h <&> (_1 %~ tail))

mkTree :: (Eq a, Ord a) => [([a], v)] -> Node a v
mkTree = mkBranch Nothing Nothing

descFromTree :: (Eq a, Ord a) => Node a v -> [([a], v)]
descFromTree root = collectTree [] root & sortDescriptions
  where
    collectTree :: (Eq a, Ord a) => [a] -> Node a v -> [([a], v)]
    collectTree addrPrefix x =
      let newAddr = addrPrefix <> (x ^. #addr <&> (: []) & fromMaybe [])
          processedRest = x ^. #children <&> collectTree newAddr & join
       in case x ^. #value of
            Just v -> (newAddr, v) : processedRest
            Nothing -> processedRest
