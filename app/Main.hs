module Main where

import Data
import Lib
import Utils

main :: IO ()
main = do
  -- printTree $ Node {addr = Just 1, value = Just "One", children = []}
  let t1Desc =
        [ ([1, 1, 9], "F"),
          ([1], "A"),
          ([2], "B"),
          ([1, 3], "C"),
          ([1, 1, 8], "D"),
          ([1, 1, 5], "H"),
          ([3, 9], "E"),
          ([1, 2, 3, 4, 5], "G")
        ]
  putStr "From tree:"
  let t1 = mkTree t1Desc
  printTree t1
  putStrLn $ "Original description: " <> show t1Desc -- (pShow t1Desc & toS)
  putStrLn $ "              sorted: " <> show (sortDescriptions t1Desc)
  putStrLn $ "        descFromTree: " <> show (descFromTree t1)
